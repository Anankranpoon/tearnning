import { AppPerson } from '../app/app-person';

export class InterfeaceStudent extends AppPerson {

  studentID: number;
  educationCertificate: string;
  department: string;

}
