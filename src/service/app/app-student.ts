import { AppPerson } from './app-person';
import { InterfeaceStudent } from '../interfect/interface-student';

export class AppStudent {
    listStuedent: InterfeaceStudent[];
    personStuedent: InterfeaceStudent;
    constructor(
      private appPerson: InterfeaceStudent
    ){

    }
    getStuedentAllList(): InterfeaceStudent[]{
      return this.listStuedent;
    }
    getStuedentByStuedentID(studentID: number): InterfeaceStudent{
      return this.listStuedent.find(item => item.studentID === studentID);
    }
    addStuedent(person: InterfeaceStudent) {
      this.listStuedent.push(person);
    }
    removeStuedent(studentID: number){
      this.listStuedent = this.listStuedent.filter(item => {
        return item.studentID !== studentID;
      });
    }
    updateStudentProfile(stuedentID: number, profile: any) {
      const stuedent =  this.getStuedentByStuedentID(stuedentID);
      Object.keys(profile).map(item => {
        stuedent[item] = profile[item];
      });

      this.listStuedent = this.listStuedent.filter(item => {
        return item.studentID !== stuedentID;
      });
      this.listStuedent.push(stuedent);
    }
}
