export class AppPerson {
  citizenID: string;
  fullName: string;
  sex: string;
  birthDay: Date;
  age: number;
  constructor() {
    this.age = this.getAge() || 0;
  }
  getAge() {
    if (this.birthDay) {
      const birthDay = new Date(this.birthDay);
      const timeDiff = Math.abs(Date.now() - birthDay.getDate());
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
  }
}
