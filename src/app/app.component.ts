import { Component } from '@angular/core';
import { AppStudent } from 'src/service/app/app-student';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tearnning-team-app';
  constructor(
    private appStudent: AppStudent
  ){

  }
}
